<?php
/**
 * The Template for displaying product archives, including the main shop page which is a post type archive
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/archive-product.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce/Templates
 * @version 3.4.0
 */

defined( 'ABSPATH' ) || exit;

get_header( 'shop' );

/**
 * Hook: woocommerce_before_main_content.
 *
 * @hooked woocommerce_output_content_wrapper - 10 (outputs opening divs for the content)
 * @hooked woocommerce_breadcrumb - 20
 * @hooked WC_Structured_Data::generate_website_data() - 30
 */


?>

<section id="page_title">
 <div class="container">
	 <div class="row">
		 <div class="col-md-12">
			 <h1>Vishwareka Hurb</h1>
            <header class="woocommerce-products-header">
                <?php if ( apply_filters( 'woocommerce_show_page_title', true ) ) : ?>
                <h2 class="woocommerce-products-header__title page-title"><?php woocommerce_page_title(); ?></h2>
                <?php endif; ?>
                <?php do_action( 'woocommerce_archive_description' );?>
            </header>
		 </div>
	 </div>
 </div>
</section>


<div class="container">
    <div class="row">
        <div class="col-sm-12">

        </div>
    </div>
    <div class="row">
        <!-- <div class="col-lg-3 col-sm-12 order-lg-1 order-2">
            <div class="left_sidebar_text">
                <h2>FILTERS</h2>
            </div>
            <?php get_template_part( 'sidebar-templates/sidebar', 'left' ); ?>
        </div> -->
        <div class="col-lg-12 col-sm-12 order-lg-2 order-1">
            <?php
            if ( woocommerce_product_loop() ) {
	            do_action( 'woocommerce_before_shop_loop' );
                    woocommerce_product_loop_start();
                    if ( wc_get_loop_prop( 'total' ) ) {
                        while ( have_posts() ) {
                            the_post();
                            do_action( 'woocommerce_shop_loop' );
                            wc_get_template_part( 'content', 'product' );
                        }
                    }
                        woocommerce_product_loop_end();
                        do_action( 'woocommerce_after_shop_loop' );
                    } else {

                        do_action( 'woocommerce_no_products_found' );
                    }
                    ?>
        </div>
    </div>
</div>
<?php 
get_footer();
