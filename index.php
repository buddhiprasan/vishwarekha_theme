<?php
get_header();
?>
<section id="page_title">
 <div class="container">
	 <div class="row">
		 <div class="col-md-12">
			 <h1>Vishwareka Hurb</h1>
				 <h2><?php the_title(); ?></h2>
		 </div>
	 </div>
 </div>
</section>

<div class="container">
<div class="row">
    <?php if ( have_posts() ) : ?>
      <?php while ( have_posts() ) : the_post(); ?>
        <div class="col-md-4">
          <?php
          get_template_part( 'loop-templates/content', get_post_format() );
          ?>
        </div>
      <?php endwhile; ?>
    <?php else : ?>
      <?php get_template_part( 'loop-templates/content', 'none' ); ?>
    <?php endif; ?>
</div>
</div>




			<!-- The pagination component -->
			<?php vishwareka_pagination(); ?>

<?php get_footer(); ?>
