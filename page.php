<?php
get_header();
?>
<section id="page_title">
 <div class="container">
	 <div class="row">
		 <div class="col-md-12">
			 <h1>Vishwareka Hurb</h1>
				 <h2><?php the_title(); ?></h2>
		 </div>
	 </div>
 </div>
</section>

<section id="page">
<div class="container">
<div class="row">
				<?php while ( have_posts() ) : the_post(); ?>
					<?php get_template_part( 'loop-templates/content', 'page' ); ?>
				<?php endwhile; // end of the loop. ?>
		</div><!-- .row -->
	</div><!-- #content -->
</section>
</div><!-- #page-wrapper -->
<?php get_footer(); ?>
