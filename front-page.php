<?php
/* Template Name: lifeguard Page */
 get_header();
?>
<?php get_template_part('page-templates/index/index', 'index_slider'); ?>

<section id="use_life_section_one">
    <div class="container">
        <div class="row">
            <div class="col-lg-6 col-md-12">
                <img class="img-fluid mx-auto d-block"
                    src="<?php echo get_template_directory_uri(); ?>/img/lifeguid.jpg" alt="">
            </div>
            <div class="col-lg-6 col-md-12">
                <h1 class="mb-3 mt-4">වෛරසයට බියනොවී ලෝකයම ජය ගමු සැවොම ආරක්ෂා වෙමු</h1>
                <p> ලෝකයම වෛරස රෝගයකට ගොදුරු වී සිටින අවධියක ජනී ජනයාගේ ජීවිත ගමන අඩාල කරමින් ඇති වී තිබෙන තත්වය
                    හමුවේ තවමත් සාර්ථක ප්‍රතිඵලයක් සොයා ගැනීමට නොහැකිව ඇත. අධර්මයෙන් හික්මුණු මිනිසා ගිල ගනිමින් තිබෙන
                    වෛරසයට ලෝකයේ තවමත්
                    සාර්ථක ප්‍රතිඵලයක් හොයා ගැනීමට නොහැකිව ඇත. </p>
                <p>මිනිසාගේ අධික මස් මාංශ හා මත්ද්‍රව්‍ය භාවිතය නිසා ශරීරය දුර්වල වීම මෙහි බලපෑම ඉහළ යෑමට හේතු වී ඇත.
                    මෙම වෛරස අභියෝගය ජය ගනිමින් මිනිසුන් හා ඔවුන්ගේ ප්‍රතිශක්තීකරණය වැඩි කරගන්නේ කෙසේද?</p>
            </div>
        </div>
    </div>
</section>

<section id="use_life_section_two">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <h2>Common Advice For Prevention of Virus</h2>
                <div class="row">
                    <div class="col-lg-3 col-md-6">
                        <div class="card p-4">
                            <img class="img-fluid mx-auto d-block"
                                src="<?php echo get_template_directory_uri(); ?>/img/New folder/corona2.jpg" alt="">
                            <h5>Wear Mask</h5>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-6 ">
                        <div class="card  p-4">
                            <img class="img-fluid mx-auto d-block"
                                src="<?php echo get_template_directory_uri(); ?>/img/New folder/corona 3.jpg" alt="">
                            <h5>Cleaning Hand</h5>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-6">
                        <div class="card  p-4">
                            <img class="img-fluid mx-auto d-block"
                                src="<?php echo get_template_directory_uri(); ?>/img/New folder/corona1.jpg" alt="">
                            <h5>Temperature Checks</h5>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-6">
                        <div class="card p-4 ">
                            <img class="img-fluid mx-auto d-block"
                                src="<?php echo get_template_directory_uri(); ?>/img/New folder/corona 4.jpg" alt="">
                            <h5>Keep Social Distancing</h5>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>



<section id="use_life_section_three">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <h2>වසර දහදාහක්ට ඉපැරණි අපේ හෙළ වෙදකම </h2>
            </div>
        </div>
        <div class="row">

            <div class="col-lg-7">
                <p>ඈත අතීතයේ පටන් අප රට තුළ ඉපැරණි වෛද්‍ය ක්‍රමය පැවතුණ බවට විශාල සාධක පවතී. බටහිර වෛද්‍ය ක්‍රමයේ
                    ආගමනයත් සහ අප රට යටත්
                    විජිතයක් බවට පත් වීමත් සමග හෙළ වෙදකම ආගාධයට පැමිණුනි. වර්තමානයේ හෙළ වෙදකම පැවතුණද ජනතාවගේ විශ්වාසය
                    බිදී ගොස් අවසානය.
                </p>
                <p>එදා බුද්ධදාස රජු කළ නාගරාජයකුගේ තිබූ ඉතා විශාල ගෙඩියක් ශෛල්‍යකර්මයකින් ඉවත් කිරීමත්, ලෝක උරුමයක් වූ
                    සීගිරිය අවම කැනීම් වලින් හමු වූ බෙහෙත් ඔරු,
                    ශෛල්‍ය වෛද්‍ය උපකරණ හා සෙල්ලිපි වල සදහන් අපේ හෙළ වෙදකම මොළයෙහි අභ්‍යන්තරයේ පැවති ගෙඩි
                    ශෛල්‍යකර්ම වලින් ඉවත් කිරීම දක්වා ඉතා විශාල දියුණුවක් ලැබුවා සේම සියලු වෛද්‍යවරුන් ඔවුන්ගේ මනස ඉහළ
                    තල වලට
                    වර්ධනය කර ගනිමින් මානසික ශක්තිය තුළින් රෝග නිරීක්ෂණය කිරීම හා නිගමනය කිරීම කළ බව සදහන් කළ යුතුය.</p>
                <p>අද වන විට පවතින වසංගත තත්වයන් වැනි ඉතා විශාල වෛරස රෝගයන් හෙළ වෙදකමින් ඉතා සාර්ථකව ජයගත බව සඳහන්
                    කරන්නේ ආඩම්බරයෙනි.</p>
            </div>
            <div class="col-lg-5">
                <img class="img-fluid mx-auto d-block"
                    src="<?php echo get_template_directory_uri(); ?>/img/history-wellness.png" alt="">
            </div>
        </div>
    </div>
</section>

<section id="use_life_section_four">
    <div class="container">
        <div class="row">
            <div class="col-lg-6">
                <img class="img-fluid mx-auto d-block"
                    src="<?php echo get_template_directory_uri(); ?>/img/lifeguard new.png" alt="">
            </div>
            <div class="col-lg-6">
                <img class="img-fluid life_logo" src="<?php echo get_template_directory_uri(); ?>/img/lifelogo.png"
                    alt="">
                <h2>Lifeguard Herbal Immunity Boosting</h2>
                <p>වසර 3000 පැරණි දේශීය වෛද්‍ය ක්‍රම යොදාගනිමින් දේශීයව නිපදවන ලද <b>Lifeguard Herbal Immunity Boosting
                    </b>
                    ප්‍රතිශක්තිකරණ ඖෂධය මේ වන විට corona මර්දන ඖෂධයක් ලෙස රෝගීන්ට ලබා දීම සිදු වෙමින් පවතී අසමොදගං,
                    ඉඟුරු,
                    අරළු, තිප්පිලි, වදකහ, වෙනිවැල්නිගැට ආදී වූ වටිනා ශාක සාරය සමග මුසු වූ ඉපැරැණි වටිනා ඖෂධ වට්ටෝරුවකින්
                    මෙය සකස් කර ඇත.</p>
                <p>මෙය භාවිතා කරන ඕනෑම කෙනෙකු වෛරස තත්ත්වයෙන් ආරක්ෂා කිරීමට හැකි අතර කිසිදු අතුරු ආබාධයකින් තොරව භාවිතා
                    කළ හැකි එකම ඖෂධය වන්නේය.
                </p>
                <p><b>ශ්‍රී ලංකා ආයුර්වේද දෙපාර්තුමේන්තුවේ අනුමැතිය සහිතයි</b></p>
                <a href="https://vishwarekhaherb.com/product/lifeguard-antiviral-herbs/">Shop Now</a>

            </div>
        </div>
    </div>
</section>

<section id="use_life_section_five">
    <div class="container">

        <div class="row justify-content-center">
            <div class="col-lg-10">
                <h2> මෙම දිව්‍යමය ඖෂධය පාවිච්චි කළ හැකි පිළිවෙල</h2>
                <ul>
                    <li> ඖෂධයෙන් තේ හැඳි එකක් නටන උණු වතුර අඩු කෝප්ප එකකට දමා දියකර විනාඩි පහක් වසා තබා පානය කරන්න.
                        ඒ අයුරින් දිනකට දෙවරක් දින 5-6 ක් පාවිච්චි කරන විට භයානක වෛරස් රෝගයන්ගෙන් සම්පූර්ණයෙන් මිදිය
                        හැක.
                        රෝගය සෑදුණු අයෙකු එසේ දින පහක් හයක් පාවිච්චි කළ විට මාසයක් දක්වා මාරාන්තික වෛරස් රෝගය ශරීරගත
                        නොවෙයි. </li>
                    <li>මෙම ඖෂධය දවසට දෙවරක්, අවම දවස් 4ත් 6ත් අතර කාලයක් පානය කළහොත්,
                        ශ්වසන රෝග වැනි දැඩි අවදානම් තත්වයන්ගෙන් දවස් 30ක ආරක්ෂාවක් ලැබෙනු ඇත.
                        දැනටමත් වෛරස් ආසාදනයකින් පෙළෙන්නේ නම්, මෙම ඖෂධය එය විනාශ කරයි.</li>
                    <li>වැඩිදුර ආරක්ෂාව සඳහා, ඉහත සඳහන් ආකාරයට සති 2කට සැරයක් භාවිතා කරන්න.</li>
                    <li>ඕනෑම සෙම් රෝගයකට යෝග්‍යයි.</li>
                    <li>වඩාත් රස වීම්ට සීනි හෝ මී පැණි මිශ්‍ර කළ හැකිය.</li>
                </ul>
                <div class="row">
                    <div class="col-lg-12">
                        <h4>අතුරු ආබාධ කිසිවක් නොමැත</h4>
                    </div>
                </div>
                <div class="card border-primary mt-4">
                    <div class="card-body border-primary text-primary">
                        <h3>විශේෂයි</h3>
                        <p class="card-text">කොරෝනා වෛරසය ශරීර ගත වී කෙසේ හෝ දිවි ගලවා ගත්තත් එම
                            වෛරසය සැඟවී ඇති සෛල පද්ධතිය තුළ දින දහයකට වඩා ක්‍රියාත්මක වී තිබුණේ නම් දීර්ඝකාලීන
                            රෝගාබාධයන්
                            රාශියකට රෝගියාට මුහුණ දීමට සිදු වන බව ලෝක සෞඛ්‍ය සංවිධානය ද තහවුරු කර ඇත.</p>
                        <p>එසේ ඇතිවන ප්‍රතිවිපාක අතර පිරිමින් සඳහා මදසරුබාවය, කාන්තාවන් සඳහා දරුඵල අහිමි වීම, අක්ෂි රෝග,
                            මොළයෙහි ආඝත රෝග, වකුගඩු රෝග හා මානව ප්‍රතිශක්ති හීනතාව දැක්විය හැකි බැවින් ප්‍රතිකාර
                            රෝගය සැදිමට අවදානම ඇති හැමෝම පාවිච්චි කර ආරක්ෂා වීම වඩාත් වටිනා බව අපේ හැඟීමයි.</p>
                    </div>
                </div>
            </div>
        </div>

    </div>
</section>

<section id="use_life_section_six">
    <div class="container">


        <div class="row justify-content-center">
            <div class="col-lg-3 col-md-4">
                <div class="card">
                    <img class="img-fluid mx-auto d-block"
                        src="<?php echo get_template_directory_uri(); ?>/img/no3-min.jpg" alt="">
                </div>
                <p>දුම්පානයෙන් වළකින්න</p>

            </div>

            <div class="col-lg-3 col-md-4">
                <div class="card">
                    <img class="img-fluid mx-auto d-block"
                        src="<?php echo get_template_directory_uri(); ?>/img/no2-min.jpg" alt="">
                </div>
                <p> මත්පැන් පානයෙන් වළකින්න</p>

            </div>
            <div class="col-lg-3 col-md-4">
                <div class="card">
                    <img class="img-fluid mx-auto d-block"
                        src="<?php echo get_template_directory_uri(); ?>/img/no 1-min.jpg" alt="">
                </div>
                <p>මාංශ ආහාරයෙන් වළකින්න</p>

            </div>
        </div>
        <div class="row">
            <div class="col-lg-12">
                <h3> ඉහත භාවිතයන් නිසා ප්‍රතිඵල අඩු විය හැක</h3>
            </div>
        </div>

    </div>
</section>

<section id="use_life_section_seven">
    <div class="container">
        <div class="row">
            <div class="col-lg-6">
                <img class="img-fluid mx-auto d-block"
                    src="<?php echo get_template_directory_uri(); ?>/img/lifeguard new.png" alt="lifeguard">
            </div>
            <div class="col-lg-6">
                <img class="img-fluid mx-auto d-block text_img"
                    src="<?php echo get_template_directory_uri(); ?>/img/text.png" alt="">
                <a class="order_btn" href="https://vishwarekhaherb.com/product/lifeguard-antiviral-herbs/">Online
                    Order</a>
                <p class="text-center"><b>ශ්‍රී ලංකා ආයුර්වේද දෙපාර්තුමේන්තුවේ අනුමැතිය සහිතයි</b></p>
                <h3>බෙදාහැරීමෙ විමසීම්</h3>
                <a class="order_call" href="tel:755003828">+94 75 500 38 28</a>
                <ul class="list-inline social_icon">
                    <li class="list-inline-item"><a target="_blank" href="https://www.facebook.com/vishwarekhaherb"><i
                                class="fab fa-facebook-square"></i> </a></li>
                    <!-- <li class="list-inline-item"> <a target="_blank" href="#"><i class="fab fa-whatsapp-square"></i></a>
                    </li> -->
                    <li class="list-inline-item"><a target="_blank"
                            href="https://www.messenger.com/t/vishwarekhaherb"><i
                                class="fab fa-facebook-messenger"></i></a></li>
                </ul>
            </div>
        </div>
    </div>

</section>


<?php get_footer(); ?>