<?php
/**
 * The template for displaying all single posts.
 *
 * @package vishwareka
 */
get_header();
?>
<section id="page_title">
 <div class="container">
	 <div class="row">
		 <div class="col-md-12">
			 <h1>Vishwareka Hurb</h1>
				 <h2><?php the_title(); ?></h2>
		 </div>
	 </div>
 </div>
</section>

<div class="container">
	<div class="row">
		<div class="col-md-8">
						<main class="singel_blog" id="main">
							<?php while ( have_posts() ) : the_post(); ?>
								<?php get_template_part( 'loop-templates/content', 'single' ); ?>
								<?php vishwareka_post_nav(); ?>
							<?php endwhile; // end of the loop. ?>
						</main><!-- #main -->
		</div>
		<div class="col-md-4">
			<div class="blog_aside">
					<?php get_sidebar('offer'); ?>
			</div>

		</div>
	</div>
</div>


<?php get_footer(); ?>
