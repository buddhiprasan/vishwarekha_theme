
<div href="#" class="blog_singel_card_link">
	<div class="blog_singel_card">
		<div class="blog_img">
			<span><?php the_tags(); ?></span>
		
			<?php if ( has_post_thumbnail()) : ?>
  <a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>" >
    <?php the_post_thumbnail(); ?>
  </a>
<?php endif; ?>

		</div>
		<div class="blog_body">
			<?php the_title( sprintf( '<h2 class="entry-title"><a href="%s" rel="bookmark">', esc_url( get_permalink() ) ),
										'</a></h2>' ); ?>
										<?php if ( 'post' == get_post_type() ) : ?>
										<?php endif; ?>
			<p><?php the_excerpt(); ?></p>
		</div>
		<div class="blog_footer">
			<a href="#"><i class="far fa-calendar-alt"></i> &nbsp;&nbsp;<?php the_date(); ?></a>
		</div>
	</div>
</div>
