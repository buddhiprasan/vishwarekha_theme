<?php
/**
 * Blank content partial template.
 *
 * @package vishwareka
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

the_content();
