<?php
 /* Template Name: oil product Template */
?>
<?php get_header(); ?>

<section id="page_title">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<h1>Vishwareka Hurb</h1>
					<h2><?php the_title(); ?></h2>
			</div>
		</div>
	</div>
</section>


<section id="oil_page">
	<div class="container">
		<div class="row">
			<div class="col-md-8">
				<div id="product_side">
					<h1>Joint Formed Oil</h1>
					<h2>100% Natural</h2>
					<img src="https://vishwarekhaherb.com/wp-content/uploads/2019/04/Layer-10-e1555656464353.png" alt="" class="img-fluid m-auto d-block">
					<p>සියලුම වාත වේදනා, හන්දි කැක්කුම්, තුනටිය කැක්කුම්, උළුක්කුවීම්, අත් පා හිරිවැටීම, මාංශ පේශි පෙරළීම, වේදනා නහර ගැට ගැසීම, පක්ෂඝාතය, සුසුම්නාවේ ආබාධ, අංශභාගය, ආතරයිටීස් ආදිය රෝගාබාධයන්ට ඉක්මන් සුවය සඳහා විශේෂයෙන් පාවිච්චි කරන්න. </p>
					<p>රසායනික ද්‍රව්‍යයන් ඇති ඛනිජ තෙල් අඩංගු නැති 100% ශාකසාර ස්වාභාවික ද්‍රව්‍ය අඩංගු වන  සන්ධි ප්‍රකෘති තෛලය.</p>
				</div>

			</div>
			<div class="col-md-4">
				<?php get_sidebar('oil'); ?>
			</div>
		</div>
	</div>
</section>

<section id="blog_index">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<div class="page_title">
					<h2><span>Healthy</span> Life</h2>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-4">
				<a href="#" class="blog_singel_card_link">
					<div class="blog_singel_card">
						<div class="blog_img">
							<span>Tips</span>
							<img src="https://www.vishwarekhaherb.com/wp-content/uploads/2019/04/image-15-740x482.jpg" alt=""  class="img-fluid">
						</div>
						<div class="blog_body">
							<h2>Tips For Living a Longer Life</h2>
							<p>Lorem ipsum dolor sit amet, legere explicari vis in, scaevola reprimique eloquentiam at eos. No labitur repudiare usu</p>
						</div>
						<div class="blog_footer">
							<a href="#"><i class="far fa-calendar-alt"></i> 25th March 2019</a>
						</div>
					</div>
				</a>
			</div>
			<div class="col-md-4">
				<a href="#" class="blog_singel_card_link">
					<div class="blog_singel_card">
						<div class="blog_img">
							<span>Tips</span>
							<img src="https://www.vishwarekhaherb.com/wp-content/uploads/2019/04/image-16-740x482.jpg" alt=""  class="img-fluid">
						</div>
						<div class="blog_body">
							<h2>Tips For Living a Longer Life</h2>
							<p>Lorem ipsum dolor sit amet, legere explicari vis in, scaevola reprimique eloquentiam at eos. No labitur repudiare usu</p>
						</div>
						<div class="blog_footer">
							<a href="#"><i class="far fa-calendar-alt"></i> 25th March 2019</a>
						</div>
					</div>
				</a>
			</div>
			<div class="col-md-4">
				<a href="#" class="blog_singel_card_link">
					<div class="blog_singel_card">
						<div class="blog_img">
							<span>Tips</span>
							<img src="https://www.vishwarekhaherb.com/wp-content/uploads/2019/04/image-17-740x482.jpg" alt=""  class="img-fluid">
						</div>
						<div class="blog_body">
							<h2>Tips For Living a Longer Life</h2>
							<p>Lorem ipsum dolor sit amet, legere explicari vis in, scaevola reprimique eloquentiam at eos. No labitur repudiare usu</p>
						</div>
						<div class="blog_footer">
							<a href="#"><i class="far fa-calendar-alt"></i> 25th March 2019</a>
						</div>
					</div>
				</a>
			</div>

		</div>
		<div class="row">
			<div class="col-md-12">
				<div class="btn_theme btn_theme_y mt-4">
					<a href="#" class="bg_yello"> Read All</a>
				</div>
			</div>
		</div>
	</div>
</section>


<?php get_footer(); ?>
