<?php get_header(); 
/* Template Name: Oil Page */
?>
<?php get_template_part('page-templates/index/index', 'index_slider'); ?>
<?php get_template_part('page-templates/index/index', 'main_icon'); ?>
<?php get_template_part('page-templates/index/index', 'index_offer'); ?>
<?php get_template_part('page-templates/index/index', 'index_product'); ?>
<?php get_template_part('page-templates/index/index', 'index_callaction'); ?>
<?php get_template_part('page-templates/index/index', 'index_whyjoin'); ?>
<?php get_template_part('page-templates/index/index', 'index_review'); ?>
<?php get_template_part('page-templates/index/index', 'blog'); ?>

<?php get_footer(); ?>