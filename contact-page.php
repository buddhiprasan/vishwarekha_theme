<?php /* Template Name: contact Template */
 get_header();
?>
<section id="page_title">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<h1>Vishwareka Hurb</h1>
					<h2><?php the_title(); ?></h2>
			</div>
		</div>
	</div>
</section>

<section id="contact_page" >
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<h1>We Are Ready To Help You 24/7</h1>
				<div class="row">
					<div class="col-md-4">
						<img src="https://vishwarekhaherb.com/wp-content/uploads/2019/04/004-pin.png" alt="" class="img-fluid">
						<h2>Location</h2>
						<a href="https://goo.gl/maps/9e41f85LXLR2">Sumaga Samadi Arana, Agalawaththa Rd, Mathugama, Sri Lanka</a>
					</div>
					<div class="col-md-4">
						<img src="https://vishwarekhaherb.com/wp-content/uploads/2019/04/001-phone.png" alt="" class="img-fluid">
						<h2>Hot Line</h2>
						<a href="tel:+94755003828">+94 75 500 38 28</a>
					</div>
					<div class="col-md-4">
						<img src="https://vishwarekhaherb.com/wp-content/uploads/2019/04/002-email.png" alt="" class="img-fluid">
						<h2>Email</h2>
						<a href="mailto:ayubowan@vishwarekhaherb.com">ayubowan@vishwarekhaherb.com</a>
					</div>
					<!-- <div class="col-md-3">
						<img src="https://www.vishwarekhaherb.com/wp-content/uploads/2019/04/003-whatsapp.png" alt="" class="img-fluid">
						<h2>Whatsapp</h2>
						<a href="+94755003828">+94 75 500 38 28</a>
					</div> -->
				</div>
			</div>
		</div>
	</div>

	<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3963.9215466228666!2d80.13016451463983!3d6.531592595275649!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3ae3ccfb3559425d%3A0xda7e975cc614c9d4!2sSumaga%20Samadhi%20Asapuwa!5e0!3m2!1sen!2slk!4v1589235139227!5m2!1sen!2slk" width="100%" height="300" frameborder="0" style="border:0;" allowfullscreen="" aria-hidden="false" tabindex="0"></iframe>
</section>

<?php get_footer(); ?>
