<?php
/**
 * Check and setup theme's default settings
 *
 * @package vishwareka
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

if ( ! function_exists( 'vishwareka_setup_theme_default_settings' ) ) {
	function vishwareka_setup_theme_default_settings() {

		// check if settings are set, if not set defaults.
		// Caution: DO NOT check existence using === always check with == .
		// Latest blog posts style.
		$vishwareka_posts_index_style = get_theme_mod( 'vishwareka_posts_index_style' );
		if ( '' == $vishwareka_posts_index_style ) {
			set_theme_mod( 'vishwareka_posts_index_style', 'default' );
		}

		// Sidebar position.
		$vishwareka_sidebar_position = get_theme_mod( 'vishwareka_sidebar_position' );
		if ( '' == $vishwareka_sidebar_position ) {
			set_theme_mod( 'vishwareka_sidebar_position', 'right' );
		}

		// Container width.
		$vishwareka_container_type = get_theme_mod( 'vishwareka_container_type' );
		if ( '' == $vishwareka_container_type ) {
			set_theme_mod( 'vishwareka_container_type', 'container' );
		}
	}
}
