<?php
/**
 * Custom hooks.
 *
 * @package vishwareka
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

if ( ! function_exists( 'vishwareka_site_info' ) ) {
	/**
	 * Add site info hook to WP hook library.
	 */
	function vishwareka_site_info() {
		do_action( 'vishwareka_site_info' );
	}
}

if ( ! function_exists( 'vishwareka_add_site_info' ) ) {
	add_action( 'vishwareka_site_info', 'vishwareka_add_site_info' );

	/**
	 * Add site info content.
	 */
	function vishwareka_add_site_info() {
		$the_theme = wp_get_theme();

		$site_info = sprintf(
			'<a href="%1$s">%2$s</a><span class="sep"> | </span>%3$s(%4$s)',
			esc_url( __( 'http://wordpress.org/', 'vishwareka' ) ),
			sprintf(
				/* translators:*/
				esc_html__( 'Proudly powered by %s', 'vishwareka' ),
				'WordPress'
			),
			sprintf( // WPCS: XSS ok.
				/* translators:*/
				esc_html__( 'Theme: %1$s by %2$s.', 'vishwareka' ),
				$the_theme->get( 'Name' ),
				'<a href="' . esc_url( __( 'http://vishwareka.com', 'vishwareka' ) ) . '">vishwareka.com</a>'
			),
			sprintf( // WPCS: XSS ok.
				/* translators:*/
				esc_html__( 'Version: %1$s', 'vishwareka' ),
				$the_theme->get( 'Version' )
			)
		);

		echo apply_filters( 'vishwareka_site_info_content', $site_info ); // WPCS: XSS ok.
	}
}
