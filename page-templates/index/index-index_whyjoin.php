<section id="why_join">
    <div class="container">
        <div class="row">
            <div class="col-lg-6 col-md-12">
                <div class="pain_main_img">
                    <img src="<?php echo get_bloginfo( 'template_directory' ); ?>/img/jointpain-min.png" alt=""
                        class="img-fluid">
                </div>
            </div>
            <div class="col-lg-6 col-md-12">
                <h2>WHY</h2>
                <div class="leaf_svg_8 swing" data-aos="fade-down" data-aos-easing="linear" data-aos-duration="900">
                    <img src="<?php echo get_bloginfo( 'template_directory' ); ?>/img/New folder/7.png" alt="">
                </div>
                <h3>Joint Pain</h3>
                <p>මිනිසාගේ සියලුම ක්‍රියාකාරීත්වය සිදු කරගත හැකි වන්නේ දෙපයින් හිටගෙන දෙඅත් උපයෝගී කරගනිමින් කොඳු
                    නාරටිය තුළින් ජවය බලය ගත්හොත් පමණි එබැවින් සන්ධි ස්ථාන ආරක්ෂා කර ගැනීම නිරෝගීව පවත්වා ගැනීමට
                    අත්‍යවශ්‍ය කාරණාවකි. ඇවිදීම, රාජකාරි කිරීම්, බර ඉසිලීම්, මෙන්ම ඕනෑම ශරීරය ක්‍රියාවලියකදී සන්ධි කොටස්
                    වේදනාවට ලක් වුවහොත් කුමක් කරන්නද?</p>
                <p>චලනය කල හකි දණහිස් සන්ධිය,වැලමිට සන්ධිය,වවළලුකර සන්ධිය,ආදි සන්ධි කොටස් මෙන්ම හිස්කබලේ ඇති ඇතැම් චලනය
                    කරන නොහැකි සන්ධි කොටස් වලින් සමන්විත වන පරිදි සන්ධි කොටස් දෙකක් මිනිස් ශරීරයේ පවතී.</p>
                <div class="read_more">
                    <a href="https://vishwarekhaherb.com/joint-formed-oil/" class="button arrow">Read More</a>
                </div>
            </div>
        </div>
    </div>
</section>