<section id="happy_customer">
    <div class="leaf_svg_9 swing" data-aos="fade-down" data-aos-easing="linear" data-aos-duration="900">
        <img src="<?php echo get_bloginfo( 'template_directory' ); ?>/img/New folder/8.png" alt="">
    </div>
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="page_title">
                    <h2><span>Happy</span> Customers</h2>
                    <div class="leaf_svg_10 swing" data-aos="fade-down" data-aos-easing="linear"
                        data-aos-duration="900">
                        <img src="<?php echo get_bloginfo( 'template_directory' ); ?>/img/New folder/9.png" alt="">
                    </div>
                </div>
                <div class="review_row">
                    <div id="carouselExampleControls" class="carousel slide" data-ride="carousel">
                        <div class="carousel-inner">
                            <div class="carousel-item active">
                                <img src="https://vishwarekhaherb.com/wp-content/uploads/2020/05/harsha.png"
                                    alt="" class="img-fluid">
                                <h2>Harsha Subasinghe - Sri lanka</h2>
                                <p>It proves that researchers are moving from conventional drug development to natural
                                    forms of botanical medicine worldwide. The biological properties of these medicine
                                    gives you a permanent relief with NO side effects.</p>
                                <ul class="justify-content-center">
                                    <li><i class="fas fa-star"></i></li>
                                    <li><i class="fas fa-star"></i></li>
                                    <li><i class="fas fa-star"></i></li>
                                    <li><i class="fas fa-star"></i></li>
                                    <li><i class="fas fa-star"></i></li>
                                </ul>
                            </div>
                            <div class="carousel-item">
                                <img src="https://vishwarekhaherb.com/wp-content/uploads/2020/05/thyagi.png"
                                    alt="" class="img-fluid">
                                <h2>Thyagi Dissanayake - Sri lanka</h2>
                                <p>It proves that researchers are moving from conventional drug development to natural
                                    forms of botanical medicine worldwide. The biological properties of these medicine
                                    gives you a permanent relief with NO side effects.</p>
                                <ul class="justify-content-center">
                                    <li><i class="fas fa-star"></i></li>
                                    <li><i class="fas fa-star"></i></li>
                                    <li><i class="fas fa-star"></i></li>
                                    <li><i class="fas fa-star"></i></li>
                                    <li><i class="fas fa-star"></i></li>
                                </ul>
                            </div>
                            <div class="carousel-item">
                                <img src="https://vishwarekhaherb.com/wp-content/uploads/2020/05/74588257_2373624392734827_2377988319368708096_o.jpg"
                                    alt="" class="img-fluid">
                                <h2>Prasad Karunathilaka - Sri lanka</h2>
                                <p>I would not argue with modern doctors or modern medicine. I do respect them all. But, regardless of which branch of medicine you prescribe to, you should ensure that any medications, supplements, or treatments you are utilizing are 100% safe...</p>
                                <ul class="justify-content-center">
                                    <li><i class="fas fa-star"></i></li>
                                    <li><i class="fas fa-star"></i></li>
                                    <li><i class="fas fa-star"></i></li>
                                    <li><i class="fas fa-star"></i></li>
                                    <li><i class="fas fa-star"></i></li>
                                </ul>
                            </div>
                        </div>

                    </div>

                </div>
                <a class="carousel-control-prev" href="#carouselExampleControls" role="button" data-slide="prev">
                    <svg version="1.1" id="Capa_1" x="0px" y="0px" viewBox="0 0 477.175 477.175"
                        style="enable-background:new 0 0 477.175 477.175;" xml:space="preserve" width="100px"
                        height="100px">
                        <g>
                            <path
                                d="M145.188,238.575l215.5-215.5c5.3-5.3,5.3-13.8,0-19.1s-13.8-5.3-19.1,0l-225.1,225.1c-5.3,5.3-5.3,13.8,0,19.1l225.1,225   c2.6,2.6,6.1,4,9.5,4s6.9-1.3,9.5-4c5.3-5.3,5.3-13.8,0-19.1L145.188,238.575z"
                                fill="#FFF9F9" />
                        </g>
                    </svg>
                </a>
                <a class="carousel-control-next" href="#carouselExampleControls" role="button" data-slide="next">
                    <svg version="1.1" viewBox="0 0 129 129" enable-background="new 0 0 129 129" width="100px"
                        height="100px">
                        <g>
                            <path
                                d="m40.4,121.3c-0.8,0.8-1.8,1.2-2.9,1.2s-2.1-0.4-2.9-1.2c-1.6-1.6-1.6-4.2 0-5.8l51-51-51-51c-1.6-1.6-1.6-4.2 0-5.8 1.6-1.6 4.2-1.6 5.8,0l53.9,53.9c1.6,1.6 1.6,4.2 0,5.8l-53.9,53.9z"
                                fill="#F8F8F8" />
                        </g>
                    </svg>
                </a>
            </div>
        </div>
    </div>
    <div class="left_img">
        <img src="https://www.vishwarekhaherb.com/wp-content/uploads/2019/04/bg-2.png" alt="">
    </div>
</section>