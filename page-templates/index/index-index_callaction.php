
<section id="call_action">
    <div class="container">
        <div class="row justify-content-md-center">
            <!-- <div class="col-lg-8 d-md-none">
                <p>Integer quis tempor orci. Suspendisse potenti. Interdum et malesuada fames ac ante ipsum primis in
                    faucibus.</p>
            </div> -->
            <div class="col-lg-8 col-md-8">
                <ul class="call_list">
                    <li class="call_icon"><a href="tel:+94712517775"><i class="fas fa-phone-volume"></i></a></li>
                    <li class="call_time">
                        <h3>Week Days</h3>
                        <h5>9.00 AM - 5.00 PM</h5>
                    </li>
                    <li class="call_number"><a href="tel:+94755003828">+94 75 500 38 28</a></li>
                </ul>
            </div>
        </div>
    </div>
    <div class="yello_hr">
    </div>
</section>