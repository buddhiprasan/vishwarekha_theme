<section id="offer_section">
    <div class="container">
        <div class="row justify-content-center">
            <!-- <div class="col-md-3">
				<div class="product_img">
						<img src="<?php echo get_bloginfo( 'template_directory' ); ?>/img/box.png" alt="">
				</div>
			</div> -->
            <div class="col-lg-6 col-md-7 col-sm-12">
                <div class="dilivery_text">
                    <h2>Cash on Delivery Available</h2>
                    <h3> <i class="fas fa-truck"></i> Islandwide Delivery (2 - 5 Days)</h3>
                </div>
                <div class="leaf_svg_6 swing" data-aos="fade-down" data-aos-easing="linear" data-aos-duration="900">
                    <img src="<?php echo get_bloginfo( 'template_directory' ); ?>/img/New folder/6.png" alt="">
                </div>
            </div>
            <div class="col-lg-4 col-md-5 col-sm-12">
                <div class="buy_btn">
                    <a href="https://vishwarekhaherb.com/shop" class="btn-sonar">Buy <span>Now</span></a>
                </div>
                <div class="leaf_svg_5 swing" data-aos="fade-down" data-aos-easing="linear" data-aos-duration="900">
                    <img src="<?php echo get_bloginfo( 'template_directory' ); ?>/img/New folder/5.png" alt="">
                </div>
            </div>
        </div>
    </div>
</section>
