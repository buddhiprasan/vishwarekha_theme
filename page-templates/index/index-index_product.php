<section id="index_product">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="page_title">
                    <h2><span>Our</span> Products</h2>
                    <div class="leaf_svg_7 swing" data-aos="fade-down" data-aos-easing="linear" data-aos-duration="900">
                        <img src="<?php echo get_bloginfo( 'template_directory' ); ?>/img/New folder/7.png" alt="">
                    </div>
                </div>
            </div>
        </div>
        <div class="row justify-content-center">
            <?php
			            $args = array( 'post_type' => 'product', 'stock' => 1, 'posts_per_page' => 4, 'orderby' =>'date','order' => 'DESC' );
			            $loop = new WP_Query( $args );
			            while ( $loop->have_posts() ) : $loop->the_post(); global $product; ?>
            <div class="col-lg-4 col-md-6">
                <div id="singel_product_card" class="singel_product_card">
                    <div class="product_img">
                        <?php if ( has_post_thumbnail( $product->id ) ) {
                        $attachment_ids[0] = get_post_thumbnail_id( $product->id );
                         $attachment = wp_get_attachment_image_src($attachment_ids[0], 'full' ); ?>
                        <img src="<?php echo $attachment[0] ; ?>" class="card-image" />
                        <?php } ?>
                    </div>
                    <div class="product_name">
                        <h2><?php the_title(); ?></h2>
                        <!-- <p><?php echo apply_filters( 'woocommerce_short_description', $post->post_excerpt ); ?></p> -->
                    </div>
                    <div class="product_price">
                        <h3><?php echo $product->get_price_html(); ?></h3>
                    </div>
                    <div class="block__body">
                        <div class="btn_theme">
                            <a href="<?php the_permalink(); ?>" class="bg_yello" title="<?php the_title(); ?>"
                                id="id-<?php the_id(); ?>">Online Oder</a>
                            <a href="<?php the_permalink(); ?>" class="bg_green">View
                                Product</a>
                        </div>
                    </div>
                </div>
            </div>
            <?php endwhile; ?>
            <?php wp_reset_query(); ?>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="btn_theme btn_theme_y">
                    <a href="https://vishwarekhaherb.com/shop" class="bg_yello"> View More</a>
                </div>
            </div>
        </div>
    </div>
</section>