<section id="blog_index">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="page_title">
                    <h2><span>Healthy</span> Life</h2>
                    <div class="leaf_svg_11 swing" data-aos="fade-down" data-aos-easing="linear"
                        data-aos-duration="900">
                        <img src="<?php echo get_bloginfo( 'template_directory' ); ?>/img/New folder/10.png" alt="">
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
        <?php

$args = array(
    'posts_per_page'    => 3,
    'post_type'     => 'post',  //choose post type here
    'order' => 'DESC',
);

// The Query
$query = new WP_Query( $args );

// The Loop
if ( $query->have_posts() ) {
    while ( $query->have_posts() ) {
        $query->the_post();
?>
<!-- article -->
<div class="col-lg-4 col-md-6">
    <a href="<?php the_permalink(); ?>" class="blog_singel_card_link">
        <div class="blog_singel_card">
            <div class="blog_img">
                <span>
                <?php 
       $post_tags = get_the_tags();
       if ( $post_tags ) {
           echo $post_tags[0]->name; 
       }
                
                ?>
              
                </span>
                <?php if ( has_post_thumbnail()) : // Check if thumbnail exists ?>
                <a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>" class="h-entry__image-link">
                    <?php the_post_thumbnail(); // Declare pixel size you need inside the array ?>
                </a>
                <?php endif; ?>
            </div>
            <div class="blog_body">
                <h2><a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>"><?php the_title(); ?></a></h2>
                <p><?php the_excerpt();?></p>
            </div>
            <div class="blog_footer">
                <a href="#"><i class="far fa-calendar-alt"></i> <time datetime="<?php the_time('Y-m-j'); ?>"
                        class="dt-published"><?php the_time('jS F Y'); ?></time></a>
            </div>
        </div>
    </a>
</div>
<?php
    }
} else {
    // no posts found
}

// Restore original Post Data
wp_reset_postdata();

?>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="btn_theme btn_theme_y mt-4">
                    <a href="https://vishwarekhaherb.com/blog" class="bg_yello"> Read All</a>
                </div>
                <div class="leaf_svg_12 swing" data-aos="fade-down" data-aos-easing="linear" data-aos-duration="900">
                    <img src="<?php echo get_bloginfo( 'template_directory' ); ?>/img/New folder/11.png" alt="">
                </div>
            </div>
        </div>
    </div>
</section>
