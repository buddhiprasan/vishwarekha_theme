<section id="pain_tab">
    <div class="container">
        <div class="row justify-content-md-center align-items-center">
            <div class="col-md-10">
                <ul class="nav nav-tabs justify-content-center" id="myTab" role="tablist">
                    <li class="nav-item">
                        <a class="nav-link active" id="keen-tab" data-toggle="tab" href="#keen" role="tab"
                            aria-controls="keen" aria-selected="true">Knee pain</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" id="houlder-tab" data-toggle="tab" href="#houlder" role="tab"
                            aria-controls="houlder" aria-selected="false">Shoulder pain</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" id="back-tab" data-toggle="tab" href="#back" role="tab" aria-controls="back"
                            aria-selected="false">Back pain</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" id="neck-tab" data-toggle="tab" href="#neck" role="tab" aria-controls="neck"
                            aria-selected="false">Neck pain</a>
                    </li>
                </ul>
                <div class="tab-content" id="myTabContent">
                    <div class="tab-pane fade show active" id="keen" role="tabpanel" aria-labelledby="keen-tab">
                        <div class="row">
                            <div class="col-md-4">
                                <div class="pain_img">
                                    <img src="https://vishwarekhaherb.com/wp-content/uploads/2019/04/keenpain-1.jpg"
                                        alt="">
                                </div>
                            </div>
                            <div class="col-md-8">
                                <h2>Knee pain</h2>
                                <h3>Knee pain is a common issue affecting all age groups.</h3>
                                <p>Knee pain is a common complaint that affects people of all ages. Knee pain may be the
                                    result of an injury, such as a ruptured ligament or torn cartilage. Medical
                                    conditions — including arthritis, gout and infections — also can cause knee pain.
                                </p>
                                <div class="read_more">
                                    <a  href="https://vishwarekhaherb.com/joint-formed-oil/" class="button arrow">Read More</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="tab-pane fade" id="houlder" role="tabpanel" aria-labelledby="houlder-tab">
                        <div class="row">
                            <div class="col-md-4">
                                <div class="pain_img">
                                    <img src="https://vishwarekhaherb.com/wp-content/uploads/2019/04/sholder.jpg"
                                        alt="">
                                </div>
                            </div>
                            <div class="col-md-8">
                                <h2>Shoulder pain</h2>
                                <h3>Shoulder injuries are the most common causes of shoulder pain.</h3>
                                <p>Sometimes shoulder pain is the result of injury to another location in your body,
                                    usually the neck or bicep. ... Other causes of shoulder pain include several forms
                                    of arthritis, torn cartilage, or a torn rotator cuff. Swelling of the bursa sacs
                                    (which protect the shoulder) or tendons can also cause pain.</p>
                                <div class="read_more">
                                    <a  href="https://vishwarekhaherb.com/joint-formed-oil/" class="button arrow">Read More</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="tab-pane fade" id="back" role="tabpanel" aria-labelledby="back-tab"
                        data-aos="fade-left">
                        <div class="row">
                            <div class="col-md-4">
                                <div class="pain_img">
                                    <img src="https://vishwarekhaherb.com/wp-content/uploads/2019/04/pain2.jpg" alt="">
                                </div>
                            </div>
                            <div class="col-md-8">
                                <h2>Back pain</h2>
                                <h3>Back pain can be caused by injury, activity, or certain medical conditions.</h3>
                                <p>Conditions commonly linked to back pain include: Muscle or ligament strain. Repeated
                                    heavy lifting or a sudden awkward movement can strain back muscles and spinal
                                    ligaments.</p>
                                <div class="read_more">
                                    <a  href="https://vishwarekhaherb.com/joint-formed-oil/" class="button arrow">Read More</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="tab-pane fade" id="neck" role="tabpanel" aria-labelledby="neck-tab">
                        <div class="row">
                            <div class="col-md-4">
                                <div class="pain_img">
                                    <img src="https://vishwarekhaherb.com/wp-content/uploads/2019/04/nik.jpg" alt="">
                                </div>
                            </div>
                            <div class="col-md-8">
                                <h2>Neck pain</h2>
                                <h3>Neck pain is common, but it is not usually a sign of arthritis.</h3>
                                <p>Your neck is made up of vertebrae that extend from the skull to the upper torso.
                                    Cervical discs absorb shock between the bones. The bones, ligaments, and muscles of
                                    your neck support your head and allow for motion.</p>
                                <div class="read_more">
                                    <a  href="https://vishwarekhaherb.com/joint-formed-oil/" class="button arrow">Read More</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="leaf_svg_2 swing" data-aos="fade-down" data-aos-easing="linear" data-aos-duration="900">
                <img src="<?php echo get_bloginfo( 'template_directory' ); ?>/img/New folder/2.png" alt="">
            </div>
            <div class="leaf_svg_3 swing" data-aos="fade-down" data-aos-easing="linear" data-aos-duration="1200">
                <img src="<?php echo get_bloginfo( 'template_directory' ); ?>/img/New folder/3.png" alt="">
            </div>
        </div>
    </div>
</section>