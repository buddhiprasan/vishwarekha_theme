<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the #content div and all content after
 *
 * @package vishwareka
 */


?>
<!-- <a id="back2Top" title="Back to top" href="#">&#10148;</a> -->
<footer class="footer">
    <div class="container">
        <div class="row">
            <div class="col-md-5">
                <!-- <div class="footer_barnd_logo">
                    <img src="https://www.vishwarekhaherb.com/wp-content/uploads/2019/04/VISHWAREKHA-HERBAL-PRODUCTS-copy-2.png"
                        alt="">
                </div> -->
                <div class="office_info">
                    <h2><span>Head</span> Office</h2>
                    <ul>
                        <li>
                            <ul class="line_two">
                                <li><i class="fa fa-map-marker-alt"></i></li>
                                <li class="address_list"><a class="position-absolute">Samadhi Arana,
                                        Agalawaththa Rd,<br> Mathugama, Sri Lanka.</a></li>
                            </ul>
                        </li>
                        <li>
                            <ul class="line_two">
                                <li><i class="fas fa-mobile-alt"></i></li>
                                <li><a href="tel:+94755003828">+94 75 500 38 28</a></li>
                            </ul>
                        </li>
                        <li>
                            <ul class="line_two">
                                <li><i class="fas fa-envelope"></i></li>
                                <li><a href="mailto:ayubowan@vishwarekhaherb.com">ayubowan@vishwarekhaherb.com</a></li>
                            </ul>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="col-md-4">
                <div class="social_link">

                    <h2><span>Read</span> Link</h2>
                    <ul class="menu_link">
                        <li><a href="https://vishwarekhaherb.com/about-us/">About</a></li>
                        <li><a href="https://vishwarekhaherb.com/privacy-policy/">Privacy Policy</a></li>
                        <li><a href="https://vishwarekhaherb.com/refund-policy/">Return Policy</a></li>
                        <li><a href="https://vishwarekhaherb.com/terms-and-conditions/">Terms & Conditions</a></li>
                        <li><a href="https://vishwarekhaherb.com/order-tracking/">Order Tracking</a></li>
                    </ul>
                </div>
            </div>
            <div class="col-md-3">
                <div class="social_link">
                    <h2><span>Follow</span> US</h2>
                    <ul>
                        <li><a href="https://www.facebook.com/vishwarekhaherb"><i class="fab fa-facebook-square"></i></a></li>
                        <!-- <li><a href="#">><i class="fab fa-youtube"></i></a></li>
                        <li><a href="#">><i class="fab fa-twitter-square"></i></a></li> -->
                    </ul>
                </div>
                <!-- <div class="product_footer">
					<h2><span>New</span> Product</h2>
					<div class="footer_product_card">
						<div class="product_img">
							<img src="https://vishwarekhaherb.com/wp-content/uploads/2019/04/Layer-2-copy-5.png" alt="">
						</div>
						<div class="product_cap">
							<h3>Joint Formed Oil</h3>
							<h4>100% Natural</h4>
							<div class="btn_theme btn_theme_y">
								<a href="#" class="bg_yello"> Shop Now</a>
							</div>
						</div>

					</div>
				</div> -->

            </div>
        </div>
    </div>
    <div class="copy_write">
        <p>Vishwareka <?php echo date('Y'); ?> © All Rights Reserved By<a href="https://www.facebook.com/buddhiprasan/"> Buddhi Prasan</a></p>
    </div>
</footer>
<?php wp_footer(); ?>
<script>
AOS.init();

jQuery(function($) {
    $(window).scroll(function() {
        var height = $(window).scrollTop();
        if (height > 100) {
            $('#back2Top').fadeIn();
        } else {
            $('#back2Top').fadeOut();
        }
    });
    $(document).ready(function() {
        $("#back2Top").click(function(event) {
            event.preventDefault();
            $("html, body").animate({
                scrollTop: 0
            }, "slow");
            return false;
        });

    });
});
	

</script>



</body>

</html>